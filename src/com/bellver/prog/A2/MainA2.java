package com.bellver.prog.A2;

import java.io.*;

public class MainA2 {
    public static void main(String[] args) throws IOException {

        File fitxer1 = new File("resources", "fitxer1.txt");

        String cadena;
        FileReader fileReader = new FileReader(fitxer1);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        cadena = bufferedReader.readLine();

        System.out.print(cadena);

        bufferedReader.close();

    }

}

