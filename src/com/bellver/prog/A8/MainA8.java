package com.bellver.prog.A8;

import java.io.*;
import java.util.ArrayList;

public class MainA8 {
    public static int NUMDNIS = 20;

    public static void main(String[] args) throws IOException {

        File dnis = new File("resources", "dnis.txt");
        File nifs = new File("resources", "nifs.txt");

        getNifs(createDni(dnis), nifs);

    }

    public static File createDni(File file) throws IOException {

        ArrayList<String> dnis = new ArrayList<>();

        for (int x = 0; x < NUMDNIS; x++) {

            int random = (int) (Math.random() * 100000000);

            String aux = random + "";

            dnis.add(getValidDni(aux));

        }

        insertCadenes(dnis, file);

        return file;

    }

    public static String getValidDni(String dni) {

        final int DNILENGHT = 8;

        if (dni.length() < DNILENGHT) {

            int aux = DNILENGHT - dni.length();
            String zeros = "";

            for (int y = 0; y < aux; y++) {

                zeros += "0";

            }

            dni = zeros.concat(dni);
            return dni;
        }

        return dni;

    }

    public static void getNifs(File dnis, File nifs) throws IOException {

        String letras = "TRWAGMYFPDXBNJZSQVHLCKE";
        ArrayList<String> arrayNifs = new ArrayList<>();

        FileReader fileReader = new FileReader(dnis);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String cadena;

        while ((cadena = bufferedReader.readLine()) != null) {

            char letra = letras.charAt(Integer.parseInt(cadena) % 23);
            String aux = cadena + "-" + letra;

            arrayNifs.add(aux);

        }

        insertCadenes(arrayNifs, nifs);

    }

    public static void insertCadenes(ArrayList<String> cadenes, File mergedFile) throws IOException {


        FileWriter fileWriter = new FileWriter(mergedFile);
        PrintWriter printWriter = new PrintWriter(fileWriter);

        for (String cadena : cadenes) {

            printWriter.println(cadena);

        }

        printWriter.close();

    }

}
