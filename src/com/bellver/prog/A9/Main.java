package com.bellver.prog.A9;

import com.bellver.prog.A9.Catalogue.Catalogue;
import com.bellver.prog.A9.Catalogue.FileProductRepository;


public class Main {

    public static FileProductRepository fileProductRepository = new FileProductRepository();

    public static void main(String[] args) {

        Enterprise enterprise = new Enterprise(getDefaultCatalogue());
        enterprise.show();

    }

    private static Catalogue getDefaultCatalogue(){

        Catalogue catalogue = new Catalogue(fileProductRepository);

        return catalogue;

    }
}
