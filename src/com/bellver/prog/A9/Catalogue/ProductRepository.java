package com.bellver.prog.A9.Catalogue;

import java.util.ArrayList;

public interface ProductRepository {

    public ArrayList<Product> findAll();

    public void save(Product product);

    public Product findById(String productId);

}
