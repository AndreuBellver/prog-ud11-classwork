package com.bellver.prog.A5;

import java.io.*;
import java.util.ArrayList;

public class MainA5 {
    public static void main(String[] args) throws IOException {

        File file1 = new File("resources", "file1");
        File file2 = new File("resources", "file2");

        createUnifiedFile(file1, file2);

    }

    public static void createUnifiedFile(File file1, File file2) throws IOException {

        File mergedFile = new File("resources", "mergedFile");

        ArrayList<String> aux = new ArrayList<>();

        ArrayList<String> auxArray1 = takeInfFile(file1);
        ArrayList<String> auxArray2 = takeInfFile(file2);



        for (String cadena:auxArray1) {



        }
        insertCadenes(aux, mergedFile);

    }

    public static ArrayList takeInfFile(File file) throws IOException {

        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        ArrayList<String> aux = new ArrayList<>();
        String cadena;

        while ((cadena = bufferedReader.readLine()) != null) {

            aux.add(cadena);

        }

        return aux;

    }

    public static void insertCadenes(ArrayList<String> cadenes, File mergedFile) throws IOException {


        FileWriter fileWriter = new FileWriter(mergedFile);
        PrintWriter printWriter = new PrintWriter(fileWriter);

        for (String cadena : cadenes) {

            printWriter.println(cadena);
        }

        printWriter.close();

    }

}
