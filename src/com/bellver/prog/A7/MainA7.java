package com.bellver.prog.A7;

import java.io.*;
import java.util.ArrayList;

public class MainA7 {
    public static void main(String[] args) throws IOException {

        File file7 = new File("resources", "file7.txt");

        filterLines(5, file7);

    }

    public static void filterLines(int numChar, File file) throws IOException {

        File newFile = new File("resources", "filer.txt");

        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String cadena;
        ArrayList<String> aux = new ArrayList<>();

        while ((cadena = bufferedReader.readLine()) != null) {

            if (cadena.length() >= numChar) {

                aux.add(cadena);

            }

        }

        insertCadenes(aux, newFile);

    }

    public static void insertCadenes(ArrayList<String> cadenes, File mergedFile) throws IOException {


        FileWriter fileWriter = new FileWriter(mergedFile);
        PrintWriter printWriter = new PrintWriter(fileWriter);

        for (String cadena : cadenes) {

            printWriter.println(cadena);
        }

        printWriter.close();

    }

}
