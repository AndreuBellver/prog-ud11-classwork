package com.bellver.prog.A4;

import com.bellver.prog.A1.MainA1;

import java.io.*;

public class MainA4 {

    public static File alumnesClasse = new File("resources", "fitxer4.txt");
    public static FileWriter fileWriter;
    public static BufferedReader bufferedReader;
    public static FileReader fileReader;
    public static PrintWriter printWriter;
    public static String[] alumnestoInsert = new String[]{"Andreu Bellver Ferrando", "Albero Valles Pastor", "Nicolas Mengual Barber"};


    public static void main(String[] args) throws IOException {

        MainA1.crearFitxer("resources", "fitxer4.txt");

        insertAlumnes(alumnestoInsert);
        eliminarAlumne(alumnestoInsert[1]);

    }

    public static void insertAlumnes(String[] alumnes) throws IOException {

        fileWriter = new FileWriter(alumnesClasse);
        printWriter = new PrintWriter(fileWriter);

        for (int x = 0; x < alumnes.length; x++) {

            printWriter.println(alumnes[x]);

        }

        printWriter.close();

    }

    public static void eliminarAlumne(String alumne) throws IOException {

        String[] newAlumnes = new String[alumnestoInsert.length - 1];
        String cadena;

        fileReader = new FileReader(alumnesClasse);
        bufferedReader = new BufferedReader(fileReader);
        fileWriter = new FileWriter(alumnesClasse);
        printWriter = new PrintWriter(fileWriter);

        for (int y = 0; y < alumnestoInsert.length; y++) {

            if (!alumnestoInsert[y].equalsIgnoreCase(alumne)) {

                if (y == 0) {

                    newAlumnes[y] = alumnestoInsert[y];

                } else {

                    newAlumnes[y - 1] = alumnestoInsert[y];

                }

            }

        }

        while ((cadena = bufferedReader.readLine()) != null) {

            if (cadena.equalsIgnoreCase(alumne)) {

                printWriter.print("");

            } else {

            }

            printWriter.close();
        }

        insertAlumnes(newAlumnes);

    }

}
