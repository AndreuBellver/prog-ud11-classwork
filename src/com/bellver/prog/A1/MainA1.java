package com.bellver.prog.A1;


import java.io.File;
import java.io.IOException;

public class MainA1 {
    public static void main(String[] args) throws IOException {

        crearFitxer("resources", "fitxer1.txt");
        veureContingut("resources");

        veureInf("resources", "fitxer1.txt");

    }

    public static void crearFitxer(String directori, String fitxer) throws IOException {

        try {

            File fitxer1 = new File(directori, fitxer);

            if (!fitxer1.exists()) {

                fitxer1.createNewFile();

            } else {

                System.out.println("El fichero ya existe. Cambia el nombre para no suscribir el ya existente.\n");
            }

        } catch (IOException e) {

            System.out.println("El disco esta lleno o no tienes permisos de escritura.\n");
            throw e;
        }

    }

    public static void veureContingut(String directori) {


        File aux = new File(directori);
        String[] dir = aux.list();

        if (dir.length == 0) {

            System.out.println("El directorio esta vacio.\n");

        } else {

            for (int x = 0; x < dir.length; x++) {

                System.out.println(dir[x]);

            }
            System.out.println();
        }

    }

    public static void veureInf(String directori, String fitxer)  {


            File auxFile = new File(directori,fitxer);

            System.out.println("nom: " + auxFile.getName());
            System.out.println("ruta absoluta: " + auxFile.getAbsolutePath());
            System.out.println("ruta relativa: " + auxFile.getPath());
            System.out.println("es pot llegir: " + auxFile.canRead());
            System.out.println("es pot escriure:  " + auxFile.canWrite());
            System.out.println("grandaria: " + auxFile.length());
            System.out.println("es un fitxer: " + auxFile.isFile());
            System.out.println("es un directori: " + auxFile.isDirectory());
        }

}
