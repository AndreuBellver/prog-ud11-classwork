package com.bellver.prog.A3;

import com.bellver.prog.A1.MainA1;

import java.io.*;

public class MainA3 {
    public static void main (String[] args) throws IOException {

        File fitxer3 = new File("resources", "fitxer3.txt");

        MainA1.crearFitxer("resources", "fitxer3.txt");

        String cadena;
        FileReader fileReader = new FileReader(fitxer3);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        while((cadena = bufferedReader.readLine())!=null) {

            System.out.println(cadena);

        }

        bufferedReader.close();
    }

}
